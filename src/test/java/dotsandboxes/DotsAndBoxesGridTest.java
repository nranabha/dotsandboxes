package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import java.beans.Transient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Code to test that boxComplete returns true for boxes that are complete
     */
    @Test
    public void boxCompleteDetectsCompletedBoxes(){
        DotsAndBoxesGrid tGrid = new DotsAndBoxesGrid(5, 5, 2);
        tGrid.drawHorizontal(0, 0, 0);
        tGrid.drawHorizontal(0, 1, 0);
        tGrid.drawVertical(0, 0, 0);
        tGrid.drawVertical(1, 0, 0);
        assertTrue(tGrid.boxComplete(0, 0));
    }
    
    /*
     * Code to test that boxComplete returns false for boxes that are not complete
     */
    @Test
    public void boxCompleteDetectsIncompleteBoxes(){
        DotsAndBoxesGrid tBoxesGrid = new DotsAndBoxesGrid(5,5 , 2);
        assertFalse(tBoxesGrid.boxComplete(0, 0));
        tBoxesGrid.drawHorizontal(0, 0, 0);
        assertFalse(tBoxesGrid.boxComplete(0, 0));
        tBoxesGrid.drawHorizontal(0, 1, 0);
        assertFalse(tBoxesGrid.boxComplete(0, 0));
        tBoxesGrid.drawVertical(0, 0, 0);
        assertFalse(tBoxesGrid.boxComplete(0, 0));
        
    }
    /*
     * Code to test if a line is drawn on top of an existing line is detected
     */
    @Test
    public void drawMethodsDetectRedrawnLines(){
        DotsAndBoxesGrid tGrid = new DotsAndBoxesGrid(5, 5, 2);
        tGrid.drawHorizontal(0, 0, 0);
        assertThrows(IllegalStateException.class, () -> {
            tGrid.drawHorizontal(0, 0, 0);
        });

        tGrid.drawVertical(0, 0, 0);
        assertThrows(IllegalStateException.class, () -> {
            tGrid.drawVertical(0, 0, 0);
        });
    }
}
